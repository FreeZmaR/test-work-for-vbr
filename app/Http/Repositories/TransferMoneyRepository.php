<?php


namespace App\Http\Repositories;


use App\Http\Contracts\Repositories\MoneyTransferRepositoryContract;
use Illuminate\Database\Eloquent\Model;

class TransferMoneyRepository extends BaseRepository implements MoneyTransferRepositoryContract
{

    public function initTransaction($senderUserId, $transferUserId, $amount)
    {
        return $this->getModel()->create([
            'sender_user_id' => $senderUserId,
            'transfer_user_id' => $transferUserId,
            'amount' => $amount,
            'status' => 'in_progress',
        ]);
    }

    public function acceptTransaction($token)
    {
       $transaction = $this->getTransactionByToken($token);

       $transaction->status = 'success';

       $transaction->save();
    }

    public function rejectTransaction($token, $rejectMessage)
    {
        $transaction = $this->getTransactionByToken($token);

        $transaction->status = 'reject';
        $transaction->message = $rejectMessage;

        $transaction->save();
    }

    public function getTransaction($transactionId): Model
    {
        return $this->getModel()->findOrFail($transactionId);
    }

    public function getTransactionByToken($token): Model
    {
        return $this->getModel()::where('token', $token)->findOrFail();
    }
}
