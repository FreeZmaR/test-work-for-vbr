<?php


namespace App\Http\Repositories;


use App\Http\Contracts\Repositories\RepositoryContract;
use Illuminate\Database\Eloquent\Model;

class BaseRepository implements RepositoryContract
{
    protected Model $_model;
    public function __construct(Model $model)
    {
        $this->_model = $model;
    }

    public function getModel(): Model
    {
        return $this->_model;
    }
}
