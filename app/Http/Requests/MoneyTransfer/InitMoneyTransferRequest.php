<?php


namespace App\Http\Requests\MoneyTransfer;


use Illuminate\Foundation\Http\FormRequest;

class InitMoneyTransferRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'transfer_user_id' => 'required', // I would use uuid
            'amount' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'transfer_user_id.required' => 'User is not selected',
            'amount.required' => 'Amount not specified',
            'amount.numeric' => 'Invalid value',
        ];
    }
}
