<?php


namespace App\Http\Requests\MoneyTransfer;


use Illuminate\Foundation\Http\FormRequest;

class MoneyTransferInfoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'transaction_id' => 'required|exists:transactions,id',
        ];
    }

    public function messages()
    {
        return [
            'transaction_id.required' => 'Transaction not selected',
            'transaction_id.exists' => 'Transaction not found',
        ];
    }
}
