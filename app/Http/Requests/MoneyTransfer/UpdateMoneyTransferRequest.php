<?php


namespace App\Http\Requests\MoneyTransfer;


use Illuminate\Foundation\Http\FormRequest;

class UpdateMoneyTransferRequest extends FormRequest
{
    public function authorize()
    {
        return false;
    }

    public function rules()
    {
        return [
            'token' => 'required|exists:transactions,token',
            'status' => 'required|boolean',
            'message' => 'string|nullable',
        ];
    }

    public function messages()
    {
        return [
            'token.required' => 'Token not token',
            'token.exists' => 'Token is not exists',
            'status.required' => 'Status not specified',
            'status.boolean' => 'Invalid value',
            'message.string' => 'Invalid value',
        ];
    }
}
