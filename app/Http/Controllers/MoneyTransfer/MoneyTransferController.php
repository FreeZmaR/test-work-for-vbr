<?php


namespace App\Http\Controllers\MoneyTransfer;


use App\Http\Contracts\Services\MoneyTransferServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\MoneyTransfer\InitMoneyTransferRequest;
use App\Http\Requests\MoneyTransfer\MoneyTransferInfoRequest;
use App\Http\Requests\MoneyTransfer\UpdateMoneyTransferRequest;
use Illuminate\Support\Facades\Auth;

class MoneyTransferController extends Controller
{
    public function initMoneyTransfer(InitMoneyTransferRequest $request)
    {
        $result = resolve(MoneyTransferServiceContract::class)
            ->initAmountMoneyTransfer(
                Auth::user(),
                $request->get('transfer_user_id'),
                $request->get('amount'),
            );

        return response()->json(['transaction_token' => $result]);
    }

    public function updateMoneyTransfer(UpdateMoneyTransferRequest $request)
    {
        $result = resolve(MoneyTransferServiceContract::class)
            ->updateTransactionStatus(
                $request->get('token'),
                $request->get('status'),
                $request->get('message'),
            );
    }

    public function getMoneyTransferInfo(MoneyTransferInfoRequest $request)
    {
        $result = resolve(MoneyTransferServiceContract::class)
            ->getTransactionInfoByTransactionId($request->get('transaction_id'));

        return response()->json(['transaction' => $result]);
    }
}
