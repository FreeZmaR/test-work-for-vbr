<?php


namespace App\Http\Services;


use App\Http\Contracts\Services\MoneyTransferServiceContract;
use App\Http\Repositories\TransferMoneyRepository;
use Exception;
use Illuminate\Database\Eloquent\Model;

class MoneyTransferService implements MoneyTransferServiceContract
{
    public function initAmountMoneyTransfer(Model $user, $transferUserId, $amount)
    {
        if (!$this->checkUserAmount($user, $amount)) {
            throw new Exception('Amount not valid');
        }

        //TODO: check transfer user in repo users
        try {
            $transaction = resolve(TransferMoneyRepository::class)
                ->initTransaction($user->getAttribute('id'));
        } catch (Exception $e)
        {
            throw new Exception('Transaction init error');
        }


        return $transaction->get('id');
    }

    public function updateTransactionStatus($token, bool $status, string $massage = '')
    {
        try {

            if ($status) {
                resolve(TransferMoneyRepository::class)->acceptTransaction($token);
            }else {
                resolve(TransferMoneyRepository::class)->rejectTransaction($token, $massage);
            }
        }catch (Exception $e)
        {
            throw new Exception('Transaction update error');
        }

        return true;
    }

    public function getTransactionInfoByTransactionId($transactionId): array
    {
        try {
            $result = resolve(TransferMoneyRepository::class)->getTransaction($transactionId);
        }catch (Exception $e)
        {
            throw new Exception('Transaction find error');
        }

        return $result;
    }

    public function checkUserAmount(Model $user, $amount): bool
    {
        return $user->getAttribute('balance') < $amount;
    }

}
