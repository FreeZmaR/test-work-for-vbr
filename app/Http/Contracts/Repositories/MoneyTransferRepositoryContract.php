<?php


namespace App\Http\Contracts\Repositories;


use Illuminate\Database\Eloquent\Model;

interface MoneyTransferRepositoryContract
{
    public function initTransaction($senderUserId, $transferUserId, $amount);
    public function acceptTransaction($token);
    public function rejectTransaction($token, $rejectMessage);
    public function getTransaction($transactionId): Model;
    public function getTransactionByToken($token): Model;
}
