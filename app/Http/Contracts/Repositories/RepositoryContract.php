<?php


namespace App\Http\Contracts\Repositories;


use Illuminate\Database\Eloquent\Model;

interface RepositoryContract
{
    public function getModel(): Model;
}
