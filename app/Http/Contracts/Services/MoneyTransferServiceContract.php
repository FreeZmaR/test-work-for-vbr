<?php


namespace App\Http\Contracts\Services;


use Illuminate\Database\Eloquent\Model;

interface MoneyTransferServiceContract
{
    public function initAmountMoneyTransfer(Model $user, $transferUserId, $amount);
    public function updateTransactionStatus($token, bool $status, String $massage = '');
    public function getTransactionInfoByTransactionId($transactionId): array;
    public function checkUserAmount(Model $user, $amount): bool;
}
