<?php


namespace App\Models;


class Transaction extends \Illuminate\Database\Eloquent\Model
{
    protected $fillable = [
        'sender_user_id',
        'transfer_user_id',
        'amount',
        'status',
        'token',
        'message',
    ];
}
