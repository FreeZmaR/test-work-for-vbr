<?php

namespace App\Providers;

use App\Http\Contracts\Repositories\MoneyTransferRepositoryContract;
use App\Http\Contracts\Services\MoneyTransferServiceContract;
use App\Http\Repositories\TransferMoneyRepository;
use App\Http\Services\MoneyTransferService;
use App\Models\Transaction;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerServices();
        $this->registerRepositories();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    private function registerServices()
    {
        $this->app->singleton(MoneyTransferServiceContract::class, function () {
            return new MoneyTransferService();
        });
    }

    private function registerRepositories()
    {
        $this->app->bind(MoneyTransferRepositoryContract::class, function () {
            return new TransferMoneyRepository(new Transaction());
        });
    }
}
