<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api'], 'namespace' => 'MoneyTransfer', 'prefix' => 'api'], function () {
    Route::post('transfer/init', 'MoneyTransferController@initMoneyTransfer')
        ->name('api.money.transfer.init');

    Route::post('transfer/update', 'MoneyTransferController@updateMoneyTransfer')
        ->name('api.money.transfer.update');

    Route::post('transfer', 'MoneyTransferController@getMoneyTransferInfo')
        ->name('api.money.transfer.info');
});
